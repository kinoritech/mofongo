__author__ = 'Horacio Hoyos'
__copyright__ = 'Copyright , Nagasu Technologies'

import os
from unittest import TestCase
from mofongo.presentation import csvmodel
from mofongo import mte


class Domain:

    def __init__(self, types):
        self.types = types

users_fn = os.path.join(os.path.dirname(__file__), 'presentation/MOCK_DATA.csv')
countries_fn = os.path.join(os.path.dirname(__file__), 'presentation/COUNTRIES.csv')
users = csvmodel.CsvModel("users", users_fn)
countries = csvmodel.CsvModel("countries", countries_fn, fieldnames=["country", "users"])

classkeys = {
    users.rowclass: ['first_name', 'last_name'],
    countries.rowclass: ['country']
}


class RowToRow:
    fn = 'fn'
    ln = 'ln'
    c = 'c'
    domain = {
        users: {
            users.rowclass: {
                'first_name': fn,
                'last_name': ln,
                'country': c},
        },
        countries: {
            countries.rowclass: {
                'country': c,
                'users': '[fn+" "+ln]'}
        }
    }

    def given(self):
        return True

    def implies(self):
        pass


class TestCsvPresentation(TestCase):

    def test_pattern(self):
        users.load()
        engine = mte.MtlEngine([users, countries], [RowToRow], users, classkeys)
        engine.execute()
        countries.store()



