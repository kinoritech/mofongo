__author__ = 'Horacio Hoyos'
__copyright__ = 'Copyright ${YERAR}, Nagasu Technologies'

import os
from unittest import TestCase
from mofongo.presentation import csvmodel


class TestCsvModel(TestCase):

    def testwithheaders(self):
        users_fn = os.path.join(os.path.dirname(__file__), 'MOCK_DATA.csv')
        m = csvmodel.CsvModel("users", users_fn)
        m.load()
        row = m.getallofkind(csvmodel.Row)[0]
        self.assertIsInstance(row, csvmodel.Row)
        self.assertTrue(hasattr(row, "id"))
        self.assertTrue(hasattr(row, "first_name"))
        self.assertTrue(hasattr(row, "last_name"))
        self.assertTrue(hasattr(row, "email"))
        self.assertTrue(hasattr(row, "country"))
        self.assertTrue(hasattr(row, "ip_address"))
        self.assertEqual(row.id, "1")
        self.assertEqual(row.first_name, "Craig")
        self.assertEqual(row.last_name, "Moore")
        self.assertEqual(row.email, "cmoore0@bizjournals.com")
        self.assertEqual(row.country, "New Caledonia")
        self.assertEqual(row.ip_address, "150.211.249.201")

    def testwithnoheaders(self):
        users_fn = os.path.join(os.path.dirname(__file__), 'MOCK_DATA_NO_HEADERS.csv')
        m = csvmodel.CsvModel("users", users_fn, noheaders=True)
        m.load()
        row = m.getallofkind(csvmodel.Row)[0]
        self.assertIsInstance(row, csvmodel.Row)
        self.assertTrue(hasattr(row, "field_0"))
        self.assertTrue(hasattr(row, "field_1"))
        self.assertTrue(hasattr(row, "field_2"))
        self.assertTrue(hasattr(row, "field_3"))
        self.assertTrue(hasattr(row, "field_4"))
        self.assertTrue(hasattr(row, "field_5"))
        self.assertEqual(row.field_0, "1")
        self.assertEqual(row.field_1, "Peter")
        self.assertEqual(row.field_2, "Carter")
        self.assertEqual(row.field_3, "pcarter0@jugem.jp")
        self.assertEqual(row.field_4, "China")
        self.assertEqual(row.field_5, "26.177.133.38")

    def testwithexternalheaders(self):
        users_fn = os.path.join(os.path.dirname(__file__), 'MOCK_DATA_NO_HEADERS.csv')
        m = csvmodel.CsvModel("users", users_fn, fieldnames=["id", "first_name", "last_name", "email", "country",
                                                             "ip_address"], noheaders=True)
        m.load()
        row = m.getallofkind(csvmodel.Row)[0]
        self.assertIsInstance(row, csvmodel.Row)
        self.assertTrue(hasattr(row, "id"))
        self.assertTrue(hasattr(row, "first_name"))
        self.assertTrue(hasattr(row, "last_name"))
        self.assertTrue(hasattr(row, "email"))
        self.assertTrue(hasattr(row, "country"))
        self.assertTrue(hasattr(row, "ip_address"))
        self.assertEqual(row.id, "1")
        self.assertEqual(row.first_name, "Peter")
        self.assertEqual(row.last_name, "Carter")
        self.assertEqual(row.email, "pcarter0@jugem.jp")
        self.assertEqual(row.country, "China")
        self.assertEqual(row.ip_address, "26.177.133.38")

    def testmorefields(self):
        users_fn = os.path.join(os.path.dirname(__file__), 'MOCK_DATA_VARARGS.csv')
        m = csvmodel.CsvModel("users", users_fn, restkey="country")
        m.load()
        row = m.getallofkind(csvmodel.Row)[0]
        self.assertIsInstance(row, csvmodel.Row)
        self.assertTrue(hasattr(row, "id"))
        self.assertTrue(hasattr(row, "first_name"))
        self.assertTrue(hasattr(row, "last_name"))
        self.assertTrue(hasattr(row, "email"))
        self.assertEqual(row.id, "1")
        self.assertEqual(row.first_name, "Robert")
        self.assertEqual(row.last_name, "Daniels")
        self.assertEqual(row.email, "rdaniels0@hibu.com")
        self.assertTrue("Russia" in row.country)
        self.assertTrue("Nigeria" in row.country)
        self.assertTrue("France" in row.country)

    def testlessfields(self):
        users_fn = os.path.join(os.path.dirname(__file__), 'MOCK_DATA_EMPTY_FIELDS.csv')
        m = csvmodel.CsvModel("users", users_fn, restval="Worcester")
        m.load()
        row = m.getallofkind(csvmodel.Row)[0]
        self.assertIsInstance(row, csvmodel.Row)
        self.assertTrue(hasattr(row, "id"))
        self.assertTrue(hasattr(row, "first_name"))
        self.assertTrue(hasattr(row, "last_name"))
        self.assertTrue(hasattr(row, "email"))
        self.assertTrue(hasattr(row, "country"))
        self.assertTrue(hasattr(row, "city"))
        self.assertEqual(row.id, "1")
        self.assertEqual(row.first_name, "Chris")
        self.assertEqual(row.last_name, "Jordan")
        self.assertEqual(row.email, "cjordan0@furl.net")
        self.assertEqual(row.country, "Indonesia")
        self.assertEqual(row.city, "Worcester")
        row = m.getallofkind(csvmodel.Row)[1]
        self.assertEqual(row.id, "2")
        self.assertEqual(row.first_name, "Edward")
        self.assertEqual(row.last_name, "Williamson")
        self.assertEqual(row.email, "ewilliamson1@disqus.com")
        self.assertEqual(row.country, "Brazil")
        self.assertEqual(row.city, "Vila Velha")

    def teststore(self):
        users_fn = os.path.join(os.path.dirname(__file__), 'MOCK_DATA.csv')
        m = csvmodel.CsvModel("users", users_fn)
        m.load()
        row = m.getallofkind(csvmodel.Row)[0]
        row.first_name = "Horacio"
        m.store()
        m2 = csvmodel.CsvModel("users", users_fn)
        m2.load()
        row = m2.getallofkind(csvmodel.Row)[0]
        self.assertEqual(row.first_name, "Horacio")
        row.first_name = "Craig"
        m2.store()
