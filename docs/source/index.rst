.. mofongo documentation master file, created by
   sphinx-quickstart on Sat Jul 25 21:29:47 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.
.. http://www.colourlovers.com/palette/159036/Brasil_Feeling#

mofongo Documentation
*********************

How to eat mofongo

Baby Steps
==========

* **From Scratch** :doc:`Overview <overview>`

Contents:

.. toctree::
   :maxdepth: 2

   connectivity


Coverage
========
* `Coverage report </mofongo/docs/build/coverage/index.html>`_


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

