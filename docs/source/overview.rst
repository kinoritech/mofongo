:orphan:

********
Overview
********

.. include:: ../../README.rst
    :start-line: 3

The Presentation Layer
======================

The presentation layer is responsible for the delivery of model elements as pure Python objects. The user can then
access this objects either to query information from them or to modify them. After the models have been accessed or
modified, the presentation layer is responsible for serializing the models back to their original data format.

If your model is a CSV file, then you can load each row as an object as follows::

    >>> from mofongo.presentation import csvModel

    >>> p = csvModel.CsvPresentation("/Some/Location/MOCK_DATA.csv")
    >>> m = csvModel.CsvModel("users", p)
    >>> m.load()

For CSV models, a unique class is created to represent row elements. Although it is possible to retrieve the specific
class for a given model, all model row classes hava a common Row super type. All rows can be retrieved using this class::

    >>> row = m.get_all_of_kind(csvModel.Row)[0]

Once we have acces to the rows, we can query them for information. Assuming MOCK_DATA.csv contents to be::

    id,first_name,last_name,email,country,ip_address
    1,Craig,Moore,cmoore0@bizjournals.com,New Caledonia,150.211.249.201
    2,Donna,Morales,dmorales1@sciencedirect.com,France,25.200.83.172
    3,Stephen,Gardner,sgardner2@home.pl,Portugal,199.120.113.238
    4,Teresa,Gomez,tgomez3@who.int,France,28.251.41.115

The row objects will have a matching attribute for each of the fields defined in the file::

    >>> print(row.first_name)
    Craig
    >>> print(row.country)
    New Caledonia

The attributes can also be used to modify the values of the fields of a row object::

    >>> row.first_name = 'Guido'
    >>> print(row.first_name)
    Guido

Any changes to the objects in the model can be persisted by saving the model::

    >>> m.store()

Inspection of the original CSV file reveals that the changes were persisted::

    id,first_name,last_name,email,country,ip_address
    1,Guido,Moore,cmoore0@bizjournals.com,New Caledonia,150.211.249.201
    2,Donna,Morales,dmorales1@sciencedirect.com,France,25.200.83.172
    3,Stephen,Gardner,sgardner2@home.pl,Portugal,199.120.113.238
    4,Teresa,Gomez,tgomez3@who.int,France,28.251.41.115

