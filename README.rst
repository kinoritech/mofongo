Mofongo
=======

Mofongo, standing for nothing in particular at all (perhaps MOdelling Framework), is a platform that provides services
to facilitate users to perform model management tasks such as model transformation, model comparison, model merging,
model refactoring and model validation using Python. Mofongo is inspired by `Epsilon <http://www.eclipse.org/epsilon/>`_.

You can find the documentation `here <http://mofongo.readthedocs.io/en/latest/>`_.