__author__ = 'Horacio Hoyos'
__copyright__ = 'Copyright , Nagasu Technologies'


class TypeNotFoundError(LookupError):
    """
    Raised when a model is asked to handle a type that is not present in the known types of its presentation layer.
    """

