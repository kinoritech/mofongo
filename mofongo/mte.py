__author__ = 'Horacio Hoyos'
__copyright__ = 'Copyright , Nagasu Technologies'

import inspect

'''
The global context to use when evaluating pattern expressions
'''
glob_context = {"__builtins__": None}


class MtlBase(type):

    def __new__(mcs, name, bases, dct):
        # Hook the search mechanism
        # For the moment pick the first Model in the dict
        for k, v in dct.items():
            if k == 'domain':
                print(v)
                # The domain value is a dict, each key is a model.

        # Hook the trace mechanism
        # Hook the mtl execution, so it does the search for model types and then creates an instance of the rule

        # for each permutation
        return super(MtlBase, mcs).__new__(mcs, name, bases, dct)

    def __call__(cls, *args, **kwargs):
        """
        Since the Rule class doesnt have an init method, we add the arguments after creating the object
        """
        o = type.__call__(cls)
        # Add the objects to attributes
        for key, value in kwargs.items():
            setattr(o, key, value)
        return o


class Trace:

    def __init__(self, source_object, rule):
        self.source_object = source_object
        self.rule = rule
        self.target_object = None

    def add_target_object(self, target_object):
        self.target_object = target_object


class MtlEngine:

    def __init__(self, models, rules, input_model, class_keys):
        self.models = models
        self.input_model = input_model
        self.rules = rules
        self.class_keys = class_keys

    def execute(self):
        for r in self.rules:
            print(r.domain)
            source_matches = []
            traces = []
            for d in iter(r.domain):
                if d == self.input_model:
                    print('match')
                    print(r.domain[d])
                    templates = r.domain[d]
                    # Get all the types from the model
                    source_matches, objects = self.find_bindings(templates, d)
                    break
            for ob in objects:
                trace = Trace(ob, r)
                traces.append(trace)
            # Once the matches have been found, instantiate the out put objects.
            for d in iter(r.domain):
                if d != self.input_model:
                    print('update')
                    print(r.domain[d])
                    templates = r.domain[d]
                    for patt_type, pattern in templates.items():
                        print(patt_type)
                        # Find if the pattern matches the key
                        # TODO what happens to pattern without keys?
                        if patt_type in self.class_keys:
                            keyattr = set(self.class_keys[patt_type])
                            pattattr = set(pattern.keys())
                            if keyattr.issubset(pattattr):
                                for m in source_matches:
                                    self.create_or_update(d, patt_type, pattern, keyattr, m)

    def create_or_update(self, model, patt_type, pattern, key_attr, local_context):
        #First try to find an object that matches, if no match, craete a new one.
        for to in model.getallofkind(patt_type):
            match = True
            for key in key_attr:
                value = pattern[key]
                newval = eval(value, glob_context, local_context)
                attrval = getattr(to, key)
                match &= (attrval == newval)
            if match:
                self.update_object(to, pattern, key_attr, local_context)
                break # only one match as keys make the object unique
        else:
            # Create a new instance
            newto = model.create_instance(patt_type)
            self.update_object(newto, pattern, set(), local_context)

    def update_object(self, obj, pattern, key_attr, local_context):
        for key, value in pattern.items():
            newval = eval(value, glob_context, local_context)
            if key not in key_attr:
                if isinstance(newval, str):
                    setattr(obj, key, newval)
                else:
                    try:
                        iterator = iter(newval)
                    except TypeError:
                        # not iterable
                        setattr(obj, key, newval)
                    else:
                        # TODO For collection values should we check if the value exists, before adding it
                        # iterable
                        try:
                            attrval = getattr(obj, key)
                        except AttributeError:
                            # The attribute hasn't been initialized
                            setattr(obj, key, newval)
                        else:
                            #list?
                            try:
                                attrval.extend(newval)
                            except AttributeError:
                                # set
                                attrval |= newval

    def find_bindings(self, templates, model):
        source_matches = []
        objects = []
        for ttype, pattern in templates.items():
            print(ttype)
            print(len(model.getallofkind(ttype)))
            for so in model.getallofkind(ttype):
                objects.append(so)
                match = dict()
                for key, value in pattern.items():
                    # The key is the attribute in the source object (so) and the
                    # value is the name under which the attribute is stored
                    print(key, value)
                    if isinstance(value, dict):
                        # We need some method to do this
                        # Call find bindings? Maybe more tricky because this are nested objects. Needs further investigation. E.g, for the outter to match all the inners mus match?
                        pass
                    else:
                        val = getattr(so, key)
                        print(val)
                        match[value] = val
                source_matches.append(match)
                print(source_matches)
        return source_matches, objects



class MtlRuleExecution:

    def __init__(self, rule, inputs, outputs):
        """
        Inputs/Outputs is a dict {modelName:[type1, type2, ..., typeN]}
        :param rule:
        :param inputs:
        :param outputs:
        :return:
        """
        pass



