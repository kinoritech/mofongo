"""
Each model specifies a name which must be unique in the context of the model repository in which it is contained. Also,
it defines a number of aliases; that is non-unique alternate names; via which it can be accessed. The interface also
defines the following services.


Similarly, the isTypeOf( element : Object, type : String ) and isKindOf( element : Object, type : String) return whether
the element in question has a type-of or a kind-of relationship with the type respectively. The getTypeOf( element :
Object ) method returns the fully- qualified name of the type an element conforms to.
The hasType( type : String ) method returns true if the model supports a type with the specified name. To support
technologies that enable users to define abstract (non-instantiable) types, the isInstantiable( type : String )
method returns if instances of the type can be created.
"""

import csv
from mofongo.mofongo import TypeNotFoundError


class Row:
    """
    Base class from which all specific model row classes inherit. This provides a common type for all rows.
    """
    pass


class RowMetaclass(type):
    """
    The Row Metaclass is used to instantiate the specific csv model classes.
    """

    def __call__(cls, **kwargs):
        """
        Instantiate a new object of the respective csv model class.
        :param kwargs list: The specific attributes to set in the new object based on the csv model class.
        :return: A new instance of the csv model class.
        :rtype: object
        """
        o = type.__call__(cls)
        # Add the row headers as attributes to the class
        for key, value in kwargs.items():
            setattr(o, key, value)
        return o


class CsvPresentation:
    """
    The CsvPresentation class is responsible for loading the csv file and instantiating an object for each row in the file. The instantiated objects will have one attribute per field.
    """

    @staticmethod
    def load(rowclass, path, encoding, restkey, restval, dialect, fieldnames=None, readonload=True):
        """
        Loads the csv file by creating instances of the row class for each row in the file
        :param rowclass: The class used to create the object to represent each row
        :param path: The path to the csv file
        :param encoding: The encoding in the of the csv file
        :param restkey: The attribute name used to group any data in a row that has more fields than the fieldnames sequence
        :param restval: The value to use  when a row has fewer fields than the fieldnames sequence
        :param dialect: Define a set of parameters specific to a particular CSV dialect (see
        `Dialect <https://docs.python.org/3/library/csv.html#csv.Dialect>`_
        :param fieldnames: A sequence whose elements are associated with the fields of the input data in order. It can
        be used to override the csv file fields or to provide ones for csv files without fields
        :param readonload:
        :return: A list of objects, the fieldnames of the csv file (equal to the fieldnames parameters, if given)
        :rtype: Tuple
        """
        rows = []
        if readonload:
            with open(path, encoding=encoding) as csvfile:
                if not fieldnames:
                    reader = csv.DictReader(csvfile, restkey=restkey, restval=restval, dialect=dialect)
                else:
                    reader = csv.DictReader(csvfile, fieldnames=fieldnames, restkey=restkey, restval=restval,
                                            dialect=dialect)
                for row in reader:
                    rows.append(rowclass(**row))
                fieldnames = reader.fieldnames
        return rows, fieldnames

    @staticmethod
    def store(file, encoding, restval, dialect, noheaders, fieldnames, data):
        """
        Persists a list of objects in a csv file
        :param file: The path to the csv file
        :param encoding: The encoding in the of the csv file
        :param restval: The value to use when an object doesn't has an attribute of one of the fieldnames
        :param dialect: Define a set of parameters specific to a particular CSV dialect (see
        `Dialect <https://docs.python.org/3/library/csv.html#csv.Dialect>`_
        :param noheaders: If set to true, the fieldnames are not persisted in the file
        :param data: A list with the objects to be persisted.
        """
        with open(file, mode='w', encoding=encoding, newline='') as csvfile:
            writer = csv.writer(csvfile, dialect=dialect)
            if not noheaders:
                writer.writerow(fieldnames)
            for o in data:
                row = []
                for key in fieldnames:
                    value = vars(o).get(key, restval)
                    # IF value is a list, expand it to individual fields
                    if not isinstance(value, str):
                        for u in value:
                            row.append(u)
                    else:
                        row.append(value)
                writer.writerow(row)
            csvfile.flush()


class CsvModel:
    """
    A model layer to allow mofongo to use csv files as model
    By default the CSV Model uses the first row to determine the field names in the file. For files without a
    header row, a fieldnames parameter can be provided to be used the header fields. If a header row is not present and
    no specific headers are known/wanted the noheaders parameter should be true. In this case the resulting class will
    have attributes field_0, field_1, ..., field_n

    If the row read has more fields than the header sequence (as determined from above), the remaining data is added
    as a sequence keyed by the value of restkey. If the row read has fewer fields than the fieldnames sequence, the
    remaining keys take the value of the optional restval parameter. For the restval option to work, lines with
    missing fields should not end in a comma. as this would be interpreted as an empty value.
    An optional dialect parameter can be given which is used to define a set of parameters specific to a particular
    CSV dialect. It may be an instance of a subclass of the Dialect class or one of the strings returned by the list
    dialects() function. The default dialect is excel.
    """
    def __init__(self, name, path, encoding='utf-8', fieldnames=None, noheaders=False, restkey=None, restval=None,
                 dialect='excel', aliases=set()):
        """
        Initialize a new CsvModel
        :param name: The name of the model
        :param path: The path to the csv file
        :param encoding: The encoding in the of the csv file
        :param fieldnames: A sequence whose elements are associated with the fields of the input data in order.
        :param noheaders: Set to true if the csv file has no headers, or the existing ones are to be ignored
        :param restkey: The attribute name used to group any data in a row that has more fields than the fieldnames sequence
        :param restval: The value to use  when a row has fewer fields than the fieldnames sequence
        :param dialect: Define a set of parameters specific to a particular CSV dialect (see
        `Dialect <https://docs.python.org/3/library/csv.html#csv.Dialect>`_ The default dialect is excel
        :param aliases: The name aliases of for the model
        """
        self.name = name
        self.aliases = aliases
        self.path = path
        self.encoding = encoding
        self.noheaders = noheaders
        if noheaders and not fieldnames:
            # Get the number of fields from the first row, and crate names for them:
            # field_0, field_1, ..., field_n
            with open(self.path, encoding=self.encoding) as csvfile:
                reader = csv.reader(csvfile)
                row = next(reader)
                fieldsize = len(row)
            self.fieldnames = ["field_" + str(n) for n in range(fieldsize)]
        else:
            self.fieldnames = fieldnames
        self.restkey = restkey
        self.restval = restval
        self.dialect = dialect
        self.knowntypes = dict()
        self.rowclass = RowMetaclass(name.capitalize()+"Row", (Row,), {})
        self.knowntypes[self.rowclass] = []

    def load(self, readonload=True):
        """
        Load the model
        :param readonload: Set to true if the file should be read while loading
        """
        elements, fn = CsvPresentation.load(self.rowclass, self.path, self.encoding, self.restkey, self.restval,
                                            self.dialect, self.fieldnames, readonload)
        self.knowntypes[self.rowclass].extend(elements)
        if not self.fieldnames:
            self.fieldnames = fn

    def store(self, location=None):
        """
        Store the model
        :param location: An alternative path to store the csv file
        """
        if location:
            path = location
        else:
            path = self.path
        assert len(self.knowntypes.values()) == 1       # There should be only one type in the model
        for data in self.knowntypes.values():
            CsvPresentation.store(path, fieldnames=self.fieldnames, encoding=self.encoding, restval=self.restval,
                                  dialect=self.dialect, noheaders=self.noheaders, data=data)

    def getalloftype(self, modeltype):
        """
        Get all objects in the model for the given type.
        Unless you have to specific type produced by this Model, this error will raise a TypeNotFoundError. Use
        :py:func:`CsvModel.getallofkind` pass cvsModel.Row as the argument.
        :param model_type:
        :return:
        """
        if modeltype not in self.knowntypes:
            raise TypeNotFoundError("The type {} is unknown to this model.".format(modeltype.__name__))
        return self.knwontypes[modeltype]

    def getallofkind(self, modeltype):
        """
        Get all objects in the model that are of the kind of the given type
        :param modeltype:
        :return:
        """
        for key, value in self.knowntypes.items():
            if issubclass(key, modeltype):
                return value
        raise TypeNotFoundError("The type {} is unknown to this model.".format(modeltype.__name__))

    def hastype(self, modeltype):
        """
        Note that Row is a super type of the specific model row class, and as such, this method will return false
        if Row is passed as a parameter. The specific model class can be obtained from any of the objects from
        this model.
        :param modeltype:
        :return:
        """
        return modeltype in self.get_known_types()

    def isinstantiable(self, modeltype):
        """
        Return whether the given type is instantiable in the model
        The models's row type is instantiable by default
        :param modeltype:
        :return: True if the given type is the the model's class
        """
        return self.hastype(modeltype)

    def getknowntypes(self):
        """
        The the list of known types in the model
        :return: A list containing the list of types used by the model
        """
        return self.knowntypes.keys()

    def getallcontents(self):
        """
        Get all the objects of all the types in the mdodel
        :return: A list containing all the objects
        """
        allcontents = list()
        for value in self.knowntypes.values():
            allcontents.extend(value)
        return allcontents

    def createinstance(self, modeltype):
        """
        Create a new instance of the given type
        :param modeltype: The type to instantiate
        :return: A new object of the given type
        :raises TypeNotFoundError: if the type does not exist in the model
        """
        if not self.hastype(modeltype):
            raise TypeNotFoundError("Type {1} not found in this model.".format(modeltype))
        instance = self.rowclass()
        self.knowntypes[modeltype].append(instance)
        return instance

    def delelement(self, element):
        """
        Delete an element from the model
        :param element: The element to delete
        :raises TypeNotFoundError: if the element does not exist in the list of elements of its type
        """
        try:
            existing = self.knowntypes[type(element)]
            existing.remove(element)
        except KeyError:
            raise TypeNotFoundError("Type {1} not found in this model.".format(type(element)))

