CODING CONVENTIONS
==================

TBDF


REPOSITORY
==========

mofongo uses Git as it version control system and Git Flow as the branching model.

Branch naming
~~~~~~~~~~~~~

Branch should be named using the issue/bug number, using the following naming ('#' is the issue number):

feature/#


Commit Messages
---------------

Next we describe the commit message structure/temaplate should be used for all commits to the repository. Note that since square brackets commonly used to denote optional arguments are part of the message's text, angle brackets denote optional arguments.

    [type][topicId][WIP] Git Message
    ([#issueId] [command 1] [command 2] ... [command n] [issue comment text])+

Where,
    type
        The type of commit:
            - **add**: New feature/functionality)
            - **mod**: Modifications
            - **fix**: Specific bugfix
            - **str**: Change in structure, e.g. move a file
            - **dep**: New dependency
            - **rem**: Backward incompatibility/remove
            - **rea**: Readability, e.g. fix documentation, remove commented code
            - **ref**: Refactoring
    issueId
        The issue tracking number of the issue relevant to this commit. The issue number should be prepended by a #.
    topicId
        *Future use*. Can be branch name and/or topic name for gerrit and/or topic name for reviews/discussions.
    WIP
        Work in progress/or not. Used for development location changes (e.g. switching machines) or saving work.
        The next commit should override this one (i.e. don't leave WIP commits in the repository)::

            git reset HEAD~1
            ... continue work
            git commit
            git push -f

    Git Message
        Short description of the commit
    
    [#issueId] [command 1] [command 2] ... [command n] [issue comment text]
        Details of the modification, with commands for YouTrack and comments to add to the YouTrack issue. Mulitple issues
        can be grouped by parentheses. Please reffer to YouTrack's documentation (`YouTrack Commands <https://www.jetbrains.com/help/youtrack/standalone/7.0/Apply-Commands-in-VCS-Commits.html>`_.

Examples
~~~~~~~~
::

    [add] New class to support XXX. #M-786
    
    [mod] Divided method A into two separate methods. #M-200
    
    [fix] Added test to avoid invalid address access.
    #M-261 Fixed.
    The x field is no decorated
    
    [rem] Reverting the changes to the engine until new design is completed. #M-876 To be discussed.
    
    [ref][WIP] Clean up comments. #786
